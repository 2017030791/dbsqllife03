package dataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class AgendaContacto {
    private Context context;
    private AgendaDBHelper agendaDbHelper;
    private SQLiteDatabase db;
    private String[] columnToRead = new String[]
            {
                    DefinirTabla.Contacto._ID,
                    DefinirTabla.Contacto.NOMBRE,
                    DefinirTabla.Contacto.TELEFONO1,
                    DefinirTabla.Contacto.TELEFONO2,
                    DefinirTabla.Contacto.DIRECCION,
                    DefinirTabla.Contacto.NOTAS,
                    DefinirTabla.Contacto.FAVORITO
            };
    public  AgendaContacto(Context context)
    {
        this.context = context;
        this.agendaDbHelper = new AgendaDBHelper(this.context);
    }
    public void openDatabase() {db = agendaDbHelper.getWritableDatabase();}

    public long insertContacto(Contacto c)
    {
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Contacto.NOMBRE, c.getNombre());
        values.put(DefinirTabla.Contacto.TELEFONO1, c.getTelefono1());
        values.put(DefinirTabla.Contacto.TELEFONO2, c.getTelefono2());
        values.put(DefinirTabla.Contacto.DIRECCION, c.getDireccion());
        values.put(DefinirTabla.Contacto.NOTAS, c.getNotas());
        values.put(DefinirTabla.Contacto.FAVORITO, c.isFavorite());

        return db.insert(DefinirTabla.Contacto.TABLA_NAME, null, values);
    }

    public long actualizarContacto(Contacto c,long id)
    {
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Contacto.NOMBRE, c.getNombre());
        values.put(DefinirTabla.Contacto.TELEFONO1, c.getTelefono1());
        values.put(DefinirTabla.Contacto.TELEFONO2, c.getTelefono2());
        values.put(DefinirTabla.Contacto.DIRECCION, c.getDireccion());
        values.put(DefinirTabla.Contacto.NOTAS, c.getNotas());
        values.put(DefinirTabla.Contacto.FAVORITO, c.isFavorite());

        String criterio = DefinirTabla.Contacto._ID + "=" + id;

        return db.update(DefinirTabla.Contacto.TABLA_NAME, values, criterio,null);
    }

    public long eliminarContacto(long id)
    {
        String criterio = DefinirTabla.Contacto._ID + "=" + id;
        return db.delete(DefinirTabla.Contacto.TABLA_NAME, criterio, null);
    }

    public Contacto getContacto(long id)
    {
        Contacto contacto = null;
        SQLiteDatabase db = this.agendaDbHelper.getReadableDatabase();
        Cursor c = db.query(DefinirTabla.Contacto.TABLA_NAME, columnToRead, DefinirTabla.Contacto._ID + " = ? ", new String[] {String.valueOf(id)},null,null,null);

        if (c.moveToFirst())
        {
            contacto = leerContacto(c);
        }
        c.close();
        return contacto;
    }
    public ArrayList<Contacto> allContactos()
    {
        ArrayList<Contacto> contactos = new ArrayList<~>();
        Cursor cursor = db.query(DefinirTabla.Contacto.TABLA_NAME, null, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast())
        {
            Contacto c = leerContacto(cursor);
            contactos.add(c);
            cursor.moveToNext();
        }
        cursor.close();
        return  contactos;
    }

    public void cerrar() {agendaDbHelper.close();}


}
