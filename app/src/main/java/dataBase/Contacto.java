package dataBase;

import java.io.Serializable;

public class Contacto implements Serializable {
    private long _ID;
    private String nombre;
    private String telefono1;
    private String telefono2;

    private String direccion;
    private String notas;
    private boolean favorite;
    public Contacto() {
        this._ID = 0;
        this.nombre = "";
        this.telefono1 = "";
        this.telefono2 = "";
        this.direccion = "";
        this.notas = "";
        this.favorite = favorite;
    }

    public Contacto(Contacto c) {
        this._ID = c.get_ID();
        this.nombre = c.getNombre();
        this.telefono1 = c.getTelefono1();
        this.telefono2 = c.getTelefono2();
        this.direccion = c.getDireccion();
        this.notas = c.getNotas();
        this.favorite = c.isFavorite();
    }

    public long get_ID() {
        return _ID;
    }

    public void set_ID(long _ID) {
        this._ID = _ID;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono1() {
        return telefono1;
    }

    public void setTelefono1(String telefono1) {
        this.telefono1 = telefono1;
    }

    public String getTelefono2() {
        return telefono2;
    }

    public void setTelefono2(String telefono2) {
        this.telefono2 = telefono2;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }
}

