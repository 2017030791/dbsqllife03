package dataBase;

import android.provider.BaseColumns;

public class DefinirTabla {
    public DefinirTabla() {}
    public static abstract class  Contacto implements BaseColumns
    {
        public static final String TABLE_NAME = "contactos";
        public static final String COLUMN_NAME_NOMBRE = "nombre";
        public static final String COLUMN_NAME_TELEFONO1 = "telefono1";
        public static final String COLUMN_NAME_TELEFONO2 = "telefono2";
        public static final String COLUMN_NAME_DIRECCION = "direccion";
        public static final String COLUMN_NAME_NOTAS = "notas";
        public static final String COLUMN_NAME_FAVORITE = "favorite";
    }
}
